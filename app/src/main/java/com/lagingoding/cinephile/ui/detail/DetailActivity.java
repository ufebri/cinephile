package com.lagingoding.cinephile.ui.detail;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.lagingoding.cinephile.BuildConfig;
import com.lagingoding.cinephile.R;
import com.lagingoding.cinephile.model.Movie;
import com.lagingoding.cinephile.model.TV;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity {

    @BindView(R.id.iv_detail_posterFilm)
    ImageView ivDetailPosterFilm;
    @BindView(R.id.tv_detail_titleFilm)
    TextView tvDetailTitleFilm;
    @BindView(R.id.tv_detail_overviewFilm)
    TextView tvDetailOverviewFilm;
    @BindView(R.id.tv_detail_rating)
    TextView tvDetailRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        TV tv = getIntent().getParcelableExtra("response_tv_show");
        Movie movie = getIntent().getParcelableExtra("response");
        setupDetail(tv, movie);
    }

    private void setupDetail(TV tv, Movie movie) {
        if (tv != null) {
            setupDataTV(tv);
        } else {
            setupData(movie);
        }
    }

    private void setupDataTV(TV tv) {
        tvDetailTitleFilm.setText(tv.getName());
        tvDetailOverviewFilm.setText(tv.getOverview());
        tvDetailRating.setText(String.valueOf(tv.getVoteAverage()));
        Glide.with(this)
                .load(BuildConfig.IMAGE_LINK_ORIGINAL + tv.getPosterPath())
                .into(ivDetailPosterFilm);
    }

    private void setupData(Movie movie) {
        tvDetailTitleFilm.setText(movie.getTitle());
        tvDetailOverviewFilm.setText(movie.getOverview());
        tvDetailRating.setText(String.valueOf(movie.getVoteAverage()));
        Glide.with(this)
                .load(BuildConfig.IMAGE_LINK_W500 + movie.getPosterPath())
                .into(ivDetailPosterFilm);
    }
}
