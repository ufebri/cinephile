package com.lagingoding.cinephile.ui.main.home;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.lagingoding.cinephile.R;
import com.lagingoding.cinephile.model.DataMovies;
import com.lagingoding.cinephile.model.Movie;
import com.lagingoding.cinephile.ui.detail.DetailActivity;
import com.lagingoding.cinephile.util.base.BaseViewMovie;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends Fragment implements BaseViewMovie {

    @BindView(R.id.lv_home_discovery)
    ListView lvHomeDiscovery;
    @BindView(R.id.pb_home_discovery)
    ProgressBar pbHomeDiscovery;

    private HomeAdapter adapter;
    private ArrayList<Movie> list = new ArrayList<>();
    private final String STATE_LIST = "state_list";

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupOrientation(savedInstanceState);
    }

    private void setupMVP() {
        HomePresenter presenter = new HomePresenter(this);
        presenter.getDiscovery();
    }

    @Override
    public void showData(DataMovies dataMovies) {
        if (dataMovies != null) {
            adapter = new HomeAdapter(getActivity());
            adapter.setDiscovery(dataMovies.getMovies());
            adapter.notifyDataSetChanged();
            lvHomeDiscovery.setAdapter(adapter);
            pbHomeDiscovery.setVisibility(View.GONE);
            list.addAll(dataMovies.getMovies());
            clickItem();
        } else {
            pbHomeDiscovery.setVisibility(View.GONE);
        }
    }

    private void clickItem() {
        lvHomeDiscovery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Movie movie = new Movie();
                movie.setOriginalTitle(adapter.getItem(position).getOriginalTitle());
                movie.setPosterPath(adapter.getItem(position).getPosterPath());
                movie.setOverview(adapter.getItem(position).getOverview());
                movie.setVoteAverage(adapter.getItem(position).getVoteAverage());

                startActivity(new Intent(getActivity(), DetailActivity.class)
                        .putExtra("response", movie));
            }
        });
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    private void setupOrientation(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            setupMVP();
        } else {
            pbHomeDiscovery.setVisibility(View.GONE);
            list = savedInstanceState.getParcelableArrayList(STATE_LIST);
            adapter = new HomeAdapter(getActivity());
            adapter.setDiscovery(list);
            adapter.notifyDataSetChanged();
            lvHomeDiscovery.setAdapter(adapter);
            clickItem();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(STATE_LIST, list);
    }
}
