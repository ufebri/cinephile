package com.lagingoding.cinephile.ui.main.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.lagingoding.cinephile.BuildConfig;
import com.lagingoding.cinephile.R;
import com.lagingoding.cinephile.model.Movie;

import java.util.ArrayList;
import java.util.List;

public class HomeAdapter extends BaseAdapter {

    private List<Movie> discovery = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;

    public HomeAdapter(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setDiscovery(List<Movie> items) {
        discovery = items;
        notifyDataSetChanged();
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getCount() {
        if (discovery == null) return 0;
        return discovery.size();
    }

    @Override
    public Movie getItem(int position) {
        return discovery.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_home, null);
            holder.homeTitleFilm = convertView.findViewById(R.id.tv_item_titleFilm);
            holder.homeOverViewFilm = convertView.findViewById(R.id.tv_item_overviewFilm);
            holder.homeRatingFilm = convertView.findViewById(R.id.tv_item_ratingFilm);
            holder.homePosterFilm = convertView.findViewById(R.id.iv_item_posterFilm);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.homeTitleFilm.setText(discovery.get(position).getTitle());
        holder.homeOverViewFilm.setText(discovery.get(position).getOverview());
        holder.homeRatingFilm.setText(String.valueOf(discovery.get(position).getVoteCount()));
        Glide.with(convertView)
                .load(BuildConfig.IMAGE_LINK_W500 + discovery.get(position).getPosterPath())
                .into(holder.homePosterFilm);
        return convertView;
    }

    private static class ViewHolder {
        TextView homeTitleFilm, homeOverViewFilm, homeRatingFilm;
        ImageView homePosterFilm;
    }
}
